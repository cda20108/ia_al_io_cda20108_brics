<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Contact</title>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
	integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
	crossorigin="anonymous">
	<link href="https://unpkg.com/tailwindcss/dist/tailwind.min.css" rel="stylesheet">
<style>
    @import url('https://fonts.googleapis.com/css?family=Karla:400,700&display=swap');

    .font-family-karla {
        font-family: karla;
    }
</style>
</head>
<body  class="bg-white font-family-karla h-screen">

<div class="container">
		<div class="row NAV">
			<div class="col-sm-offset-1 col-sm-10">
				<nav class="navbar navbar-expand-lg navbar-light bg-light">
					<a class="navbar-brand" href="#">BRICS</a>
					<button class="navbar-toggler" type="button" data-toggle="collapse"
						data-target="#navbarNav" aria-controls="navbarNav"
						aria-expanded="false" aria-label="Toggle navigation">
						<span class="navbar-toggler-icon"></span>
					</button>
					<div class="collapse navbar-collapse" id="navbarNav">
						<ul class="navbar-nav">
							<li class="nav-item active"><a class="nav-link" href="accueil">ACCUEIL<span
									class="sr-only">(current)</span></a></li>
							<li class="nav-item"><a class="nav-link" href="trouverBrics">TROUVER BRICS</a></li>
							
							<li class="nav-item"><a class="nav-link" href="posterOffre">POSTER OFFRE</a></li>
							
							<li class="nav-item"><a class="nav-link" href="forum">FORUM</a></li>
							
							<li class="nav-item"><a class="nav-link" href="contact">CONTACT</a>
							
							<li class="nav-item"><a class="nav-link" href="compteClient">COMPTE</a>
							
							</li>
						</ul>
					</div>
				</nav>
			</div>
		</div>
	</div>


	

        <!-- Login Section -->
        <div class="max-w-screen-lg mx-auto">


            <div class="flex flex-col justify-center md:justify-start my-auto pt-8 md:pt-0 px-8 md:px-24 lg:px-32">
                <p class="text-center text-3xl">Contactez-nous !</p>
                <form class="flex flex-col pt-3 md:pt-8" action="contact" method="post">
                
                
                    <div class="flex flex-col pt-4">
                        <label for="mail" class="text-lg">Mail</label>
                        <input type="email" name="mail" id="mail" placeholder="${ client.mail }" class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 mt-1 leading-tight focus:outline-none focus:shadow-outline">
                    </div>
                    
                    <div class="flex flex-col pt-4">
                        <label for="tel" class="text-lg">T�l�phone</label>
                        <input type="text" name="tel" id="tel" placeholder="06 51 65 78 95" class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 mt-1 leading-tight focus:outline-none focus:shadow-outline">
                    </div>
                    
                    <div class="flex flex-col pt-4">
                        <label for="lien" class="text-lg">Lien(optionnel)</label>
                        <input type="text" name="lien" id="lien" placeholder="https://..." class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 mt-1 leading-tight focus:outline-none focus:shadow-outline">
                    </div>
                    
                    <div class="flex flex-col pt-4">
                        <label for="message" class="text-lg">Message</label>
                        <textarea name="message" id="message" placeholder="Ecrivez votre message ici..." class="form-control shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 mt-1 leading-tight focus:outline-none focus:shadow-outline"></textarea>
                    </div>
    
                    <input type="submit"  value="Envoyer" class="bg-black text-white font-bold text-lg hover:bg-gray-700 p-2 mt-8">
                </form>
                
            </div>

        </div>

	


</body>
</html>