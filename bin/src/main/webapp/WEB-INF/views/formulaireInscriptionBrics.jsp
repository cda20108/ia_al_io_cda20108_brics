<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Inscription</title>
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/Form.css">
<!-- Tailwind -->
<link href="https://unpkg.com/tailwindcss/dist/tailwind.min.css"
	rel="stylesheet">
	
<style>
@import
	url('https://fonts.googleapis.com/css?family=Karla:400,700&display=swap')
	;

.font-family-karla {
	font-family: karla;
}

.multiselect {
  border: 1px #dadada solid;
}

.selectBox {
  position: relative;
}

.selectBox select {
  width: 100%;
  font-weight: bold;
}

.overSelect {
  position: absolute;
  left: 0;
  right: 0;
  top: 0;
  bottom: 0;
}

#checkboxes {
  display: none;
}

#checkboxes label {
  display: block;
}


#checkboxesTwo{
  display: none;
}

#checkboxesTwo label {
  display: block;
}

#checkboxesThree{
  display: none;
}

#checkboxesThree label {
  display: block;
}

#checkboxesFour{
  display: none;
}

#checkboxesFour label {
  display: block;
}

#checkboxesFive{
  display: none;
}

#checkboxesFive label {
  display: block;
}



</style>
</head>
<body class="bg-white font-family-karla h-screen">

	<div class="w-full flex flex-wrap">

		<!-- Register Section -->
		<div class="w-full md:w-1/2 flex flex-col">

			<div
				class="flex justify-center md:justify-start pt-12 md:pl-12 md:-mb-12">
				<a href="#" class="bg-black text-white font-bold text-xl p-4"
					alt="Logo">Brics</a>
			</div>

			<div
				class="flex flex-col justify-center md:justify-start my-auto pt-8 md:pt-0 px-8 md:px-24 lg:px-32">
				<p class="text-center text-3xl">Inscription</p>
				<form class="flex flex-col pt-3 md:pt-8" action="inscriptionBrics"
					method="post">
					<div class="flex flex-col pt-4">
						<label for="name" class="text-lg">Nom</label> <input type="text"
							id="name" name="nom" placeholder="Horn"
							class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 mt-1 leading-tight focus:outline-none focus:shadow-outline" />
					</div>

					<div class="flex flex-col pt-4">
						<label for="prenom" class="text-lg">Prenom</label> <input
							type="text" id="prenom" name="prenom" placeholder="Mike"
							class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 mt-1 leading-tight focus:outline-none focus:shadow-outline" />
					</div>

					<div class="flex flex-col pt-4">
						<label for="mail" class="text-lg">Mail</label> <input type="email"
							id="mail" name="mail" placeholder="mikehorn@gmail.com"
							class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 mt-1 leading-tight focus:outline-none focus:shadow-outline" />
					</div>

					<div class="flex flex-col pt-4">
						<label for="dateNaissance" class="text-lg">Date de
							naissance</label> <input type="text" id="dateNaissance"
							name="dateNaissance" placeholder="14/01/2000"
							class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 mt-1 leading-tight focus:outline-none focus:shadow-outline" />
					</div>

					<div class="flex flex-col pt-4">
						<label for="tel" class="text-lg">T�l�phone</label> <input
							type="text" id="tel" name="tel" placeholder="0685983533"
							class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 mt-1 leading-tight focus:outline-none focus:shadow-outline" />
					</div>

					<div class="flex flex-col pt-4">
						 <label for="distance" class="text-lg">Distance</label> 				
						 <select name="distance" id="distance" class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 mt-1 leading-tight focus:outline-none focus:shadow-outline">
						    <option value="5km">5km</option>
						    <option value="10km">10km</option>
						    <option value="20km">20km</option>
						    <option value="30km">30km</option>
						    <option value="50km">50km</option>
						    <option value="100km">100km</option>
						    <option value="200km">200km</option>
						</select>
					</div>

					<div class="flex flex-col pt-4">
						<label for="categorie" class="text-lg">Cat�gorie</label>
						
						  <div class="multiselect" class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 mt-1 leading-tight focus:outline-none focus:shadow-outline">
						    <div class="selectBox" onclick="showCheckboxes()">
						      <select>
						        <option  id="bricolage" name="bricolage">Bricolage Maison</option>
						      </select>
						      <div class="overSelect"></div>
						    </div>
						    <div id="checkboxes">
						      <label for="one">
						        <input type="checkbox" name="serrure"/>Serrurerie</label>
						      <label for="two">
						        <input type="checkbox" name="accrocheMurale" />Accroche murale</label>
						      <label for="three">
						        <input type="checkbox" name="pose"/>Pose ou r�paration store / fen�tre / porte</label>
						    </div>
						  </div>
						  
						  <div class="multiselect" >
						    <div class="selectBox" onclick="showCheckboxesTwo()">
						      <select>
						        <option>Jardinage</option>
						      </select>
						      <div class="overSelect"></div>
						    </div>
						    <div id="checkboxesTwo">
						      <label for="one">
						        <input type="checkbox" name="jardinerie"/>Jardinerie</label>
						      <label for="two">
						        <input type="checkbox" name="plante"/>Plante et semence</label>
						      <label for="three">
						        <input type="checkbox" name="portail"/>Portail</label>
						    </div>
						  </div>
						
						<div class="multiselect" >
						    <div class="selectBox" onclick="showCheckboxesThree()">
						      <select>
						        <option>Peinture</option>
						      </select>
						      <div class="overSelect"></div>
						    </div>
						    <div id="checkboxesThree">
						      <label for="one">
						        <input type="checkbox" name="toit"/>Peinture de toit</label>
						      <label for="two">
						        <input type="checkbox" name="meubles"/>Peinture de meubles</label>
						      <label for="three">
						        <input type="checkbox" name="sol_plafond"/>Peinture de sol / plafond</label>
						    </div>
						  </div>
						
						
						<div class="multiselect" >
						    <div class="selectBox" onclick="showCheckboxesFour()">
						      <select>
						        <option>Mecanique</option>
						      </select>
						      <div class="overSelect"></div>
						    </div>
						    <div id="checkboxesFour">
						      <label for="one">
						        <input type="checkbox" name="accessoire"/>Installation accessoire v�hicule</label>
						      <label for="two">
						        <input type="checkbox" name="vidange"/>Vidange</label>
						      <label for="three">
						        <input type="checkbox" name="retroviseurs"/>Installation / R�paration r�troviseurs</label>
						    </div>
						  </div>
						  
						  	<div class="multiselect" >
						    <div class="selectBox" onclick="showCheckboxesFive()">
						      <select>
						        <option>Electricite</option>
						      </select>
						      <div class="overSelect"></div>
						    </div>
						    <div id="checkboxesFive">
						      <label for="one">
						        <input type="checkbox" name="climatisation"/>Chauffage et climatisation</label>
						      <label for="two">
						        <input type="checkbox" name="gaz"/>Gaz</label>
						      <label for="three">
						        <input type="checkbox" name="energie"/>Energie renouvelable / verte</label>
						    </div>
						  </div>
						
					</div>

					<div class="flex flex-col pt-4">
						<label for="tarif" class="tarif">Tarif/heure</label> <input
							type="text" id="tarif" name="tarif" placeholder="tarif"
							class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 mt-1 leading-tight focus:outline-none focus:shadow-outline" />
					</div>



					<div class="flex flex-col pt-4">
						<label for="login" class="text-lg">Login</label> <input
							type="text" id="login" name="login" placeholder="monLog123"
							class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 mt-1 leading-tight focus:outline-none focus:shadow-outline" />
					</div>

					<div class="flex flex-col pt-4">
						<label for="mdp" class="text-lg">Mot de passe</label> <input
							type="password" id="mdp" name="mdp" placeholder="Fionad&123"
							class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 mt-1 leading-tight focus:outline-none focus:shadow-outline" />
					</div>

					<input type="submit" value="S'inscrire"
						class="bg-black text-white font-bold text-lg hover:bg-gray-700 p-2 mt-8" />
				</form>
				<div class="text-center pt-12 pb-12">
					<p>
						D�j� Inscrit ? <a href="connexionBrics"
							class="underline font-semibold">Se connecter</a>
					</p>
				</div>
			</div>

		</div>

		<!-- Image Section -->
		<div class="w-1/2 shadow-2xl">
			<img class="object-cover w-full h-screen hidden md:block"
				src="https://source.unsplash.com/IXUM4cJynP0" alt="Background" />
		</div>
	</div>

<script type="text/javascript">

var expanded = false;

function showCheckboxes() {
  var checkboxes = document.getElementById("checkboxes");
  if (!expanded) {
    checkboxes.style.display = "block";
    expanded = true;
  } else {
    checkboxes.style.display = "none";
    expanded = false;
  }
}

function showCheckboxesTwo() {
	  var checkboxes = document.getElementById("checkboxesTwo");
	  if (!expanded) {
	    checkboxes.style.display = "block";
	    expanded = true;
	  } else {
	    checkboxes.style.display = "none";
	    expanded = false;
	  }
	}
	
function showCheckboxesThree() {
	  var checkboxes = document.getElementById("checkboxesThree");
	  if (!expanded) {
	    checkboxes.style.display = "block";
	    expanded = true;
	  } else {
	    checkboxes.style.display = "none";
	    expanded = false;
	  }
	}
	
function showCheckboxesFour() {
	  var checkboxes = document.getElementById("checkboxesFour");
	  if (!expanded) {
	    checkboxes.style.display = "block";
	    expanded = true;
	  } else {
	    checkboxes.style.display = "none";
	    expanded = false;
	  }
	}
	
function showCheckboxesFive() {
	  var checkboxes = document.getElementById("checkboxesFive");
	  if (!expanded) {
	    checkboxes.style.display = "block";
	    expanded = true;
	  } else {
	    checkboxes.style.display = "none";
	    expanded = false;
	  }
	}
	
</script>

</body>
</html>