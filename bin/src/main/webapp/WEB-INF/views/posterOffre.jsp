<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
	integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
	crossorigin="anonymous">
</head>
<body>

<div class="container">
		<div class="row NAV">
			<div class="col-sm-offset-1 col-sm-10">
				<nav class="navbar navbar-expand-lg navbar-light bg-light">
					<a class="navbar-brand" href="#">BRICS</a>
					<button class="navbar-toggler" type="button" data-toggle="collapse"
						data-target="#navbarNav" aria-controls="navbarNav"
						aria-expanded="false" aria-label="Toggle navigation">
						<span class="navbar-toggler-icon"></span>
					</button>
					<div class="collapse navbar-collapse" id="navbarNav">
						<ul class="navbar-nav">
							<li class="nav-item active"><a class="nav-link" href="accueil">ACCUEIL<span
									class="sr-only">(current)</span></a></li>
							<li class="nav-item"><a class="nav-link" href="trouverBrics">TROUVER BRICS</a></li>
							
							<li class="nav-item"><a class="nav-link" href="posterOffre">POSTER OFFRE</a></li>
							
							<li class="nav-item"><a class="nav-link" href="forum">FORUM</a></li>
							
							<li class="nav-item"><a class="nav-link" href="contact">CONTACT</a>
							
							<li class="nav-item"><a class="nav-link" href="compte">COMPTE</a>
							
							</li>
						</ul>
					</div>
				</nav>
			</div>
		</div>
	</div>

	<div class="container">
		<div class="row"></div>
		<div class="row">
			<div class="col-lg-offset-3 col-lg-9">
				<form action="posterOffre" method="post">
					<div class="form-group">
						<div class="form-group">
							<label for="offreNom">Nom de l'offre</label> <input type="text"
								class="form-control" id="offreNom" name="offreNom" placeholder="Peindre salon">
						</div>
					</div>
					<div class="form-group">
						<div class="form-group">
							<label for="inputEmail4">Email</label> <input type="email"
								class="form-control" id="inputEmail4" name="inputEmail4" placeholder="Email">
						</div>
					</div>
					<div class="form-group">
						<label for="inputAddress">Adresse</label> <input type="text"
							class="form-control" id="inputAddress" name="inputAddress" placeholder="1234 Main St">
					</div>
					<div class="form-row">
						<div class="form-group col-md-6">
							<label for="inputCity">Ville</label> <input type="text"
								class="form-control" id="inputCity" name="inputCity" placeholder="Lille">
						</div>
						<div class="form-group col-md-4">
							<label for="inputState">Pays</label> <select id="inputState" name="inputState"
								class="form-control">
								<option selected>Choose...</option>
								<option>France</option>
								<option>Belgique</option>
								<option>Espagne</option>
								<option>Allemagne</option>
								<option>Suisse</option>
								<option>Italie</option>
								<option>Portugal</option>
							</select>
						</div>
						<div class="form-group col-md-2">
							<label for="inputZip">Code Postal</label> <input type="text"
								class="form-control" id="inputZip" name="inputZip"  placeholder="59000">
						</div>
					</div>
					<div class="form-row">
						<div class="form-group col-md-4">
							<label for="tarif">Tarif / Heure</label> <input type="text"
								class="form-control" id="tarif" name="tarif" placeholder="15">
						</div>
						<div class="form-group col-md-4">
							<label for="date">Date d'expiration</label> <input type="date"
								class="form-control" id="date" name="date">
						</div>
					</div>
					<div class="form-group">
						<div class="form-group ">
							<label for="description">Description</label> <textarea
								class="form-control" id="description" name="description"
								placeholder="Description..."> </textarea>
						</div>
					</div>
					<button type="submit" class="btn btn-danger">Poster</button>
				</form>
			</div>
		</div>
	</div>
</body>
</html>