<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
	integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
	crossorigin="anonymous">
</head>
<body>

<div class="container">
		<div class="row NAV">
			<div class="col-sm-offset-1 col-sm-10">
				<nav class="navbar navbar-expand-lg navbar-light bg-light">
					<a class="navbar-brand" href="accueilBrics">BRICS</a>
					<button class="navbar-toggler" type="button" data-toggle="collapse"
						data-target="#navbarNav" aria-controls="navbarNav"
						aria-expanded="false" aria-label="Toggle navigation">
						<span class="navbar-toggler-icon"></span>
					</button>
					<div class="collapse navbar-collapse" id="navbarNav">
						<ul class="navbar-nav">
							<li class="nav-item active"><a class="nav-link" href="accueilBrics">ACCUEIL<span
									class="sr-only">(current)</span></a></li>
							
							<li class="nav-item"><a class="nav-link" href="posterOffre">POSTER OFFRE</a></li>
							
							<li class="nav-item"><a class="nav-link" href="forum">FORUM</a></li>
							
							<li class="nav-item"><a class="nav-link" href="contact">CONTACT</a>
							
							<li class="nav-item"><a class="nav-link" href="compteClient">COMPTE</a>
							
							</li>
						</ul>
					</div>
				</nav>
			</div>
		</div>
	</div>


<c:set var = "brics" scope = "session" value = "${brics}"/>

<c:out value="${brics.nom}"/> <br/>
<c:out value="${brics.prenom}"/>  <br/>
<c:out value="${brics.mail}"/>  <br/>
<c:out value="${brics.dateNaissance}"/>  <br/>
<c:out value="${brics.tel}"/>  <br/>
<c:out value="${brics.distance}"/>  <br/>
<c:out value="${brics.tarif}"/> 
<c:out value="${'euros/heure'}"/> <br/>

<c:forEach var="categorie" items="${categories}">
		       <tr class="bg-white lg:hover:bg-gray-100 flex lg:table-row flex-row lg:flex-row flex-wrap lg:flex-no-wrap mb-10 lg:mb-0">
		           <td class="w-full lg:w-auto p-3 text-gray-800 text-center border border-b block lg:table-cell relative lg:static">
		               <span class="lg:hidden absolute top-0 left-0 bg-blue-200 px-2 py-1 text-xs font-bold uppercase">  </span>
		               <c:out value="${categorie.nom}"/>
		           </td>
		           <td class="w-full lg:w-auto p-3 text-gray-800 text-center border border-b text-center block lg:table-cell relative lg:static">
		               <span class="lg:hidden absolute top-0 left-0 bg-blue-200 px-2 py-1 text-xs font-bold uppercase"></span>
		               <c:forEach var="sousCategorie" items="${categorie.sousCategorie}">
		                <br/>
		               <c:out value="${sousCategorie}"/>
		               
		               
						</c:forEach>
		           </td>
		           
		       </tr>

		</c:forEach>


<p>
<a href="modifierBrics?id=${ brics.id }">modifier</a>  
<a href="supprimerBrics?id=${ brics.id }">supprimer</a>
</p>


</body>
</html>