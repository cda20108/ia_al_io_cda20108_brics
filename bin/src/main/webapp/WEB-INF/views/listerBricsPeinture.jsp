<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Trouver Offres</title>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
	integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
	crossorigin="anonymous">
<!-- Tailwind -->
<link href="https://unpkg.com/tailwindcss/dist/tailwind.min.css"
	rel="stylesheet">
<style>
@import
	url('https://fonts.googleapis.com/css?family=Karla:400,700&display=swap')
	;

.font-family-karla {
	font-family: karla;
}
</style>
</head>
<body class="bg-white font-family-karla h-screen">

	<div class="container">
		<div class="row NAV">
			<div class="col-sm-offset-1 col-sm-10">
				<nav class="navbar navbar-expand-lg navbar-light bg-light">
					<a class="navbar-brand" href="#">BRICS</a>
					<button class="navbar-toggler" type="button" data-toggle="collapse"
						data-target="#navbarNav" aria-controls="navbarNav"
						aria-expanded="false" aria-label="Toggle navigation">
						<span class="navbar-toggler-icon"></span>
					</button>
					<div class="collapse navbar-collapse" id="navbarNav">
						<ul class="navbar-nav">
							<li class="nav-item active"><a class="nav-link"
								href="accueil">ACCUEIL<span class="sr-only">(current)</span></a></li>
							<li class="nav-item"><a class="nav-link" href="trouverBrics">TROUVER
									BRICS</a></li>

							<li class="nav-item"><a class="nav-link" href="posterOffre">POSTER
									OFFRE</a></li>

							<li class="nav-item"><a class="nav-link" href="forum">FORUM</a></li>

							<li class="nav-item"><a class="nav-link" href="contact">CONTACT</a>



							<li class="nav-item"><a class="nav-link" href="compteClient">COMPTE</a>
						</li></ul>
					</div>
				</nav>
			</div>
		</div>
	</div>


	<form class="w-full max-w-lg" action="listerPeinture" method="post">
	
		<div class="flex flex-wrap -mx-3 mb-2">
			<div class="w-full md:w-1/3 px-3 mb-6 md:mb-0">
				<label
					class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2"
					for="grid-state"> Ville </label>
				<div class="relative">
					<select
						class="block appearance-none w-full bg-gray-200 border border-gray-200 text-gray-700 py-3 px-4 pr-8 rounded leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
						id="grid-state" name="ville">
						<option>Lille</option>
						<option>Paris</option>
						<option>Marseille</option>
						<option>Grenoble</option>
						<option>Bordeaux</option>
						<option>Roubaix</option>
					</select>
					<div
						class="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-700">
						<svg class="fill-current h-4 w-4"
							xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
							<path
								d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z" /></svg>
					</div>
				</div>
			</div>
			<div class="w-full md:w-1/3 px-3 mb-6 md:mb-0">
				<label
					class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2"
					for="grid-state"> Sous-catégorie </label>
				<div class="relative">
					<select
						class="block appearance-none w-full bg-gray-200 border border-gray-200 text-gray-700 py-3 px-4 pr-8 rounded leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
						id="grid-state" name="sousCategorie">
						<option>Sous cat. 1</option>
						<option>Sous cat. 2</option>
						<option>Sous cat. 3</option>
					</select>
					<div
						class="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-700">
						<svg class="fill-current h-4 w-4"
							xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
							<path
								d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z" /></svg>
					</div>
				</div>
			</div>
			
		</div>
	</form>





</body>
</html>