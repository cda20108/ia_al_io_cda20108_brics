<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Trouver Offres</title>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
	integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
	crossorigin="anonymous">
<!-- Tailwind -->
<link href="https://unpkg.com/tailwindcss/dist/tailwind.min.css"
	rel="stylesheet">
<style>
@import
	url('https://fonts.googleapis.com/css?family=Karla:400,700&display=swap')
	;

.font-family-karla {
	font-family: karla;
}
</style>
</head>
<body
	class="font-sans antialiased text-gray-900 leading-normal tracking-wider bg-cover">



	<div class="container">
		<div class="row NAV">
			<div class="col-sm-offset-1 col-sm-10">
				<nav class="navbar navbar-expand-lg navbar-light bg-light">
					<a class="navbar-brand" href="#">BRICS</a>
					<button class="navbar-toggler" type="button" data-toggle="collapse"
						data-target="#navbarNav" aria-controls="navbarNav"
						aria-expanded="false" aria-label="Toggle navigation">
						<span class="navbar-toggler-icon"></span>
					</button>
					<div class="collapse navbar-collapse" id="navbarNav">
						<ul class="navbar-nav">
							<li class="nav-item active"><a class="nav-link"
								href="accueil">ACCUEIL<span class="sr-only">(current)</span></a></li>
							<li class="nav-item"><a class="nav-link" href="trouverOffre">TROUVER
									OFFRE</a></li>

							<li class="nav-item"><a class="nav-link" href="forum">FORUM</a></li>

							<li class="nav-item"><a class="nav-link" href="contact">CONTACT</a>
							
							<li class="nav-item"><a class="nav-link" href="compteClient">COMPTE</a>

							</li>
						</ul>
					</div>
				</nav>
			</div>
		</div>
	</div>

			<c:set var="offre" scope="session" value="${offre}" />

			<p hidden="true">${ offre.id }</p>


			<div
				class="max-w-4xl flex items-center h-auto lg:h-screen flex-wrap mx-auto my-32 lg:my-0">

				<!--Main Col-->
				<div id="profile"
					class="w-full lg:w-3/5 rounded-lg lg:rounded-l-lg lg:rounded-r-none shadow-2xl bg-white opacity-100 mx-6 lg:mx-0">


					<div class="p-4 md:p-12 text-center lg:text-left">
						<!-- Image for mobile view-->
						<div
							class="block lg:hidden rounded-full shadow-xl mx-auto -mt-16 h-48 w-48 bg-cover bg-center"></div>

						<h1 class="text-3xl font-bold pt-8 lg:pt-0">${ offre.nom }</h1>
						<div
							class="mx-auto lg:mx-0 w-4/5 pt-3 border-b-2 border-black opacity-25"></div>
						<p
							class="pt-4 text-base font-bold flex items-center justify-center lg:justify-start">
							<svg class="h-4 fill-current text-orange-700 pr-4"
								xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
								<path
									d="M9 12H1v6a2 2 0 0 0 2 2h14a2 2 0 0 0 2-2v-6h-8v2H9v-2zm0-1H0V5c0-1.1.9-2 2-2h4V2a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v1h4a2 2 0 0 1 2 2v6h-9V9H9v2zm3-8V2H8v1h4z" /></svg>
							${ offre.tarif }$/h
						</p>
						<p
							class="pt-4 text-base font-bold flex items-center justify-center lg:justify-start">
							<svg class="h-4 fill-current text-orange-700 pr-4"
								xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
								<path
									d="M9 12H1v6a2 2 0 0 0 2 2h14a2 2 0 0 0 2-2v-6h-8v2H9v-2zm0-1H0V5c0-1.1.9-2 2-2h4V2a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v1h4a2 2 0 0 1 2 2v6h-9V9H9v2zm3-8V2H8v1h4z" /></svg>
							${ offre.mail }$
						</p>
						<p
							class="pt-2 text-gray-600 text-xs lg:text-sm flex items-center justify-center lg:justify-start">
							<svg class="h-4 fill-current text-orange-700 pr-4"
								xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"></svg>
						<p>${ offre.adresse.adresse }, ${ offre.adresse.ville }[${ offre.adresse.cp }],
							${ offre.adresse.pays }<p class="pt-8 text-sm">${ offre.description }</p>

			<div class="pt-12 pb-8">
				<button class="bg-black text-white font-bold text-lg hover:bg-gray-700 p-2 mt-8 rounded" > Postuler </button> 
			</div>

			
			<!-- Use https://simpleicons.org/ to find the svg for your preferred product --> 

		</div>

	</div>
	
	<!--Img Col-->
	<div class="w-full lg:w-2/5">
		<!-- Big profile image for side bar (desktop) -->
		<img
						src="https://images.unsplash.com/photo-1585201731775-0597e1be4bfb?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=658&q=80"
						class="rounded-none lg:rounded-lg shadow-2xl hidden lg:block">
		<!-- Image from: http://unsplash.com/photos/MP0IUfwrn0A -->
		
	</div>
	
	

</div>


	
</body>
		

	
</body>
</html>