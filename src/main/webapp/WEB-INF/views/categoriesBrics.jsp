<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Inscription</title>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
	integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
	crossorigin="anonymous">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/Form.css">
<!-- Tailwind -->
<link href="https://unpkg.com/tailwindcss/dist/tailwind.min.css"
	rel="stylesheet">

<style>
@import
	url('https://fonts.googleapis.com/css?family=Karla:400,700&display=swap')
	;

.font-family-karla {
	font-family: karla;
}

.multiselect {
	border: 1px #dadada solid;
}

.selectBox {
	position: relative;
}

.selectBox select {
	width: 100%;
	font-weight: bold;
}

.overSelect {
	position: absolute;
	left: 0;
	right: 0;
	top: 0;
	bottom: 0;
}

#checkboxes {
	display: none;
}

#checkboxes label {
	display: block;
}

#checkboxesTwo {
	display: none;
}

#checkboxesTwo label {
	display: block;
}
</style>
</head>
<body class="bg-white font-family-karla h-screen">


	<div class="container">
		<div class="row NAV">
			<div class="col-sm-offset-1 col-sm-10">
				<nav class="navbar navbar-expand-lg navbar-light bg-light">
					<a class="navbar-brand" href="accueilBrics">BRICS</a>
					<button class="navbar-toggler" type="button" data-toggle="collapse"
						data-target="#navbarNav" aria-controls="navbarNav"
						aria-expanded="false" aria-label="Toggle navigation">
						<span class="navbar-toggler-icon"></span>
					</button>
					<div class="collapse navbar-collapse" id="navbarNav">
						<ul class="navbar-nav">
							<li class="nav-item active"><a class="nav-link"
								href="accueilBrics">ACCUEIL<span class="sr-only">(current)</span></a></li>
							<li class="nav-item"><a class="nav-link" href="trouverOffre">TROUVER
									OFFRE</a></li>

							<li class="nav-item"><a class="nav-link" href="forum">FORUM</a></li>

							<li class="nav-item"><a class="nav-link" href="contact">CONTACT</a>
							<li class="nav-item"><a class="nav-link" href="compteClient">COMPTE</a>

							</li>
						</ul>
					</div>
				</nav>
			</div>
		</div>
	</div>

	<c:set var="categories" scope="session" value="${categories}" />
	
	
	<div class="w-full flex flex-wrap">

		<!-- Register Section -->
		<div class="w-full md:w-1/2 flex flex-col">

			<div
				class="flex justify-center md:justify-start pt-12 md:pl-12 md:-mb-12">
				<a href="#" class="bg-black text-white font-bold text-xl p-4"
					alt="Logo">Brics</a>
			</div>

			<div
				class="flex flex-col justify-center md:justify-start my-auto pt-8 md:pt-0 px-8 md:px-24 lg:px-32">
				<p class="text-center text-3xl">Inscription</p>
				<form class="flex flex-col pt-3 md:pt-8" action="modifCategories"
					method="post">


					<div class="flex flex-col pt-4">
						<label for="categorie" class="text-lg">Cat�gorie</label>

						<div class="multiselect"
							class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 mt-1 leading-tight focus:outline-none focus:shadow-outline">
							<div class="selectBox" onclick="showCheckboxes()">
								<select>
									<option id="bricolage" name="bricolage">Bricolage
										Maison</option>
								</select>
								<div class="overSelect"></div>
							</div>
							<div id="checkboxes">
								<label for="one"> <input type="checkbox" name="serrure" />Serrurerie
								</label> <label for="two"> 
									
										<input type="checkbox" name="accrocheMurale" />Accroche murale
      
								</label> <label for="three"> <input type="checkbox" name="pose" />Pose
									ou r�paration store / fen�tre / porte
								</label>
							</div>
						</div>

						<div class="multiselect">
							<div class="selectBox" onclick="showCheckboxesTwo()">
								<select>
									<option>Jardinage</option>
								</select>
								<div class="overSelect"></div>
							</div>
							<div id="checkboxesTwo">
								<label for="one"> <input type="checkbox"
									name="jardinerie" />Jardinerie
								</label> <label for="two"> <input type="checkbox" name="plante" />Plante
									et semence
								</label> <label for="three"> <input type="checkbox"
									name="portail" />Portail
								</label>
							</div>
						</div>

						<!--
						<ul>
							<li><input type="checkbox" id="jardinage" name="jardinage" />
								<label for="jardinage">Jardinage</label></li>
							<li><input type="checkbox" id="bricolage" name="bricolage" />
								<label for="bricolage">Bricolage Maison</label></li>
							<li><input type="checkbox" id="peinture" name="peinture" />
								<label for="bricolage">Peinture</label></li>
							<li><input type="checkbox" id="electromenager" name="electromenager" />
								<label for="jardinage">Electromenager</label></li>
							<li><input type="checkbox" id="plomberie" name="plomberie" />
								<label for="bricolage">Plomberie</label></li>
							<li><input type="checkbox" id="electricite" name="electricite" />
								<label for="bricolage">Electricite</label></li>
						</ul>
						
						-->

					</div>


					<input type="submit" value="Mettre � jour"
						class="bg-black text-white font-bold text-lg hover:bg-gray-700 p-2 mt-8" />
				</form>

			</div>

		</div>


	</div>

	<script type="text/javascript">

var expanded = false;

function showCheckboxes() {
  var checkboxes = document.getElementById("checkboxes");
  if (!expanded) {
    checkboxes.style.display = "block";
    expanded = true;
  } else {
    checkboxes.style.display = "none";
    expanded = false;
  }
}

function showCheckboxesTwo() {
	  var checkboxes = document.getElementById("checkboxesTwo");
	  if (!expanded) {
	    checkboxes.style.display = "block";
	    expanded = true;
	  } else {
	    checkboxes.style.display = "none";
	    expanded = false;
	  }
	}
</script>

</body>
</html>