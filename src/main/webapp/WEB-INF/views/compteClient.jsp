<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
	integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
	crossorigin="anonymous">
</head>
<body>

<div class="container">
		<div class="row NAV">
			<div class="col-sm-offset-1 col-sm-10">
				<nav class="navbar navbar-expand-lg navbar-light bg-light">
					<a class="navbar-brand" href="#">BRICS</a>
					<button class="navbar-toggler" type="button" data-toggle="collapse"
						data-target="#navbarNav" aria-controls="navbarNav"
						aria-expanded="false" aria-label="Toggle navigation">
						<span class="navbar-toggler-icon"></span>
					</button>
					<div class="collapse navbar-collapse" id="navbarNav">
						<ul class="navbar-nav">
							<li class="nav-item active"><a class="nav-link" href="accueil">ACCUEIL<span
									class="sr-only">(current)</span></a></li>
							<li class="nav-item"><a class="nav-link" href="trouverBrics">TROUVER BRICS</a></li>
							
							<li class="nav-item"><a class="nav-link" href="posterOffre">POSTER OFFRE</a></li>
							
							<li class="nav-item"><a class="nav-link" href="forum">FORUM</a></li>
							
							<li class="nav-item"><a class="nav-link" href="contact">CONTACT</a>
							
							<li class="nav-item"><a class="nav-link" href="compteClient">COMPTE</a>
							
							</li>
						</ul>
					</div>
				</nav>
			</div>
		</div>
	</div>


<div>
	<a href="mesInfosClient">Mes Informations</a>
</div>

<div>
	<a href="mesOffresClient">Mes Offres</a>
</div>

</body>
</html>