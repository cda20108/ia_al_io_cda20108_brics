package fr.afpa.beans;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@RequiredArgsConstructor
@ToString
@Entity

public class Brics {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@NonNull
	private String nom;

	@NonNull
	private String prenom;

	@NonNull
	private String mail;
	
	@NonNull
	private String dateNaissance;
	
	@NonNull
	private String tel;
	
	@NonNull
	private String distance;
	
	@NonNull
	private String tarif;
	
	@OneToOne(cascade = {CascadeType.ALL})
	@JoinColumn(name = "auth_fk")
	private Authentification authentification;
	
}
