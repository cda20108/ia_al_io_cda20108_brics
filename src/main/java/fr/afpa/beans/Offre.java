package fr.afpa.beans;

import java.time.LocalDate;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@RequiredArgsConstructor
@ToString
@Entity

public class Offre {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@NonNull
	private String nom;

	@NonNull
	private String mail;

	@NonNull
	private Float tarif;
	
	@NonNull
	private LocalDate date;
	
	@NonNull
	private String description;
	
	@OneToOne(cascade = {CascadeType.ALL})
	@JoinColumn(name = "adr_fk")
	private Adresse adresse;
	
	@ManyToOne //(cascade = {CascadeType.ALL})
    private Client client;
	
}
