package fr.afpa.repositories.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.afpa.beans.Authentification;

public interface AuthentificationRepository extends JpaRepository<Authentification, Long>{
	
	List<Authentification> findByLoginAndMdp(String login, String mdp);
	List<Authentification> findByLogin(String login);
	
}
