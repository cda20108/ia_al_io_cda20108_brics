package fr.afpa.repositories.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.afpa.beans.Client;

public interface ClientRepository extends JpaRepository<Client, Long>{

}
