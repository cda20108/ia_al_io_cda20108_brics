package fr.afpa.repositories.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.afpa.beans.Offre;

public interface OffreRepository extends JpaRepository<Offre, Long>{

}
