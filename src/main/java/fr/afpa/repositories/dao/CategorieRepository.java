package fr.afpa.repositories.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.afpa.beans.Categorie;

public interface CategorieRepository  extends JpaRepository<Categorie, Long>{

}
