package fr.afpa.repositories.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.afpa.beans.Brics;


public interface BricsRepository extends JpaRepository<Brics, Long>{

}