package fr.afpa.brics.controller;


import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import fr.afpa.beans.Brics;
import fr.afpa.beans.Categorie;
import fr.afpa.beans.Client;
import fr.afpa.beans.Offre;
import fr.afpa.repositories.dao.BricsRepository;
import fr.afpa.repositories.dao.ClientRepository;
import fr.afpa.repositories.dao.OffreRepository;

@Controller
public class AdminController {
	
	@Autowired
	ClientRepository cr;
	

	@Autowired
	BricsRepository br;
	

	@Autowired
	OffreRepository or;
	
	@GetMapping(value = "/adminDashboard")
	public ModelAndView redirectDashboard(ModelAndView mv) {

		ArrayList<Client> clientsList = (ArrayList<Client>) cr.findAll();
		ArrayList<Brics> bricsList = (ArrayList<Brics>) br.findAll();
		ArrayList<Offre> offresList = (ArrayList<Offre>) or.findAll();

	
		
		mv.addObject("nbClients",clientsList.size());
		mv.addObject("nbBrics",bricsList.size());
		mv.addObject("nbOffres",offresList.size());
		
		mv.addObject("listClient",clientsList);
		mv.addObject("listBrics",bricsList);
		mv.addObject("listOffre",offresList);
		
		mv.setViewName("adminDashboard");
		return mv;
	}
	
	
	@GetMapping(value = "/adminBrics")
	public ModelAndView redirectDashboardBrics(ModelAndView mv) {

		ArrayList<Brics> bricsList = (ArrayList<Brics>) br.findAll();
	
		mv.addObject("nbBrics",bricsList.size());
		mv.addObject("listBrics",bricsList);
		mv.setViewName("adminBrics");
		return mv;
	}
	
	@GetMapping(value = "/adminClient")
	public ModelAndView redirectDashboardClient(ModelAndView mv) {

		ArrayList<Client> clientsList = (ArrayList<Client>) cr.findAll();
	
		mv.addObject("nbClients",clientsList.size());
		mv.addObject("listClient",clientsList);
		mv.setViewName("adminClient");
		return mv;
	}
	
	
	@GetMapping(value = "/adminOffre")
	public ModelAndView redirectDashboardOffre(ModelAndView mv) {

		ArrayList<Offre> offresList = (ArrayList<Offre>) or.findAll();

		mv.addObject("nbOffres",offresList.size());
		mv.addObject("listOffre",offresList);
		mv.setViewName("adminOffre");
		return mv;
	}
	
	@GetMapping(value = "/supprimerOffreAdmin")
	public ModelAndView supprimerOffreAdmin(ModelAndView mv,@RequestParam(value = "id") String id) {
		
		or.deleteById(Long.parseLong(id));
		or.flush();
		
		ArrayList<Offre> listOffre = (ArrayList<Offre>) or.findAll();
		
		mv.addObject("listeOffre", listOffre);
		mv.addObject("nbOffres",listOffre.size());
		mv.setViewName("adminOffre");
		return mv;
	}
	
	@GetMapping(value = "/supprimerClientAdmin")
	public ModelAndView supprimerClientAdmin(ModelAndView mv,@RequestParam(value = "id") String id) {
		

		ArrayList<Offre> listOffre = (ArrayList<Offre>) or.findAll();
		
		for (Offre offre : listOffre) {
			if (offre.getClient().getId().equals(Long.parseLong(id))) {
				or.deleteById(offre.getId());
				or.flush();
			}
		}
		
		cr.deleteById(Long.parseLong(id));
		cr.flush();
		
		ArrayList<Client> listClient = (ArrayList<Client>) cr.findAll();
		
		mv.addObject("listClient", listClient);
		mv.addObject("nbClients",listClient.size());
		mv.setViewName("adminClient");
		return mv;
	}
	
	
	@GetMapping(value = "/supprimerClientAdminDash")
	public ModelAndView supprimerClientAdminDash(ModelAndView mv,@RequestParam(value = "id") String id) {
		

		ArrayList<Offre> listOffre = (ArrayList<Offre>) or.findAll();
		
		for (Offre offre : listOffre) {
			if (offre.getClient().getId().equals(Long.parseLong(id))) {
				or.deleteById(offre.getId());
				or.flush();
			}
		}
		
		cr.deleteById(Long.parseLong(id));
		cr.flush();
		
		ArrayList<Client> listClient = (ArrayList<Client>) cr.findAll();
		
		mv.addObject("listClient", listClient);
		mv.addObject("nbClients",listClient.size());
		mv.setViewName("adminDashboard");
		return mv;
	}
	
	
	@GetMapping(value = "/supprimerBricsAdmin")
	public ModelAndView supprimerBricsAdmin(ModelAndView mv,@RequestParam(value = "id") String id) {
		
		br.deleteById(Long.parseLong(id));
		br.flush();
		
		ArrayList<Brics> listBrics = (ArrayList<Brics>) br.findAll();
		
		mv.addObject("listBrics", listBrics);
		mv.addObject("nbBrics",listBrics.size());
		mv.setViewName("adminBrics");
		return mv;
	}
	
	@GetMapping(value = "/supprimerBricsAdminDash")
	public ModelAndView supprimerBricsAdminDash(ModelAndView mv,@RequestParam(value = "id") String id) {
		
		br.deleteById(Long.parseLong(id));
		br.flush();
		
		ArrayList<Brics> listBrics = (ArrayList<Brics>) br.findAll();
		
		mv.addObject("listBrics", listBrics);
		mv.addObject("nbBrics",listBrics.size());
		mv.setViewName("adminDashboard");
		return mv;
	}
	
	@GetMapping(value = "/deconnexion")
	public String redirectDeco() {
		return "home";
	}
	
}
