package fr.afpa.brics.controller;

import java.time.LocalDate;
import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import fr.afpa.beans.Adresse;
import fr.afpa.beans.Authentification;
import fr.afpa.beans.Client;
import fr.afpa.beans.Offre;
import fr.afpa.repositories.dao.AuthentificationRepository;
import fr.afpa.repositories.dao.ClientRepository;
import fr.afpa.repositories.dao.OffreRepository;

@Controller
public class OffreController {

	@Autowired
	ClientRepository cr;
	
	@Autowired
	OffreRepository or;
	
	
	@GetMapping(value = "/posterOffre")
	public String redirectFormPoster() {
		return "posterOffre";
	}
	
	@GetMapping(value = "/mesOffresClient")
	public ModelAndView mesOffres(ModelAndView mv) {
		ArrayList<Offre> listOffre = (ArrayList<Offre>) or.findAll();
		ArrayList<Offre> listFinal = new ArrayList<Offre>();
		
		
		for (Offre offre : listOffre) {
			if (offre.getClient().getId() == ClientController.getClient().getId()) {
				listFinal.add(offre);
			}
		}
		
		
		mv.addObject("listeOffre", listFinal);
		mv.setViewName("mesOffres");
		return mv;
	}
	
	@GetMapping(value = "/trouverOffre")
	public ModelAndView trouverOffreRedirect(ModelAndView mv) {
		ArrayList<Offre> listOffre = (ArrayList<Offre>) or.findAll();
		
		mv.addObject("listeOffre", listOffre);
		mv.setViewName("trouverOffre");
		return mv;
	}
	
	
	@GetMapping(value = "/modifierOffre")
	public ModelAndView modifierOffreRedirect(ModelAndView mv, @RequestParam(value = "id") String id) {
		
		Offre offre = or.findById(Long.parseLong(id)).get();
		
		mv.addObject("offre", offre);
		mv.setViewName("modifierOffre");
		return mv;
	}
	
	@GetMapping(value = "/voirOffre")
	public ModelAndView voirOffreRedirect(ModelAndView mv, @RequestParam(value = "id") String id) {
		
		Offre offre = or.findById(Long.parseLong(id)).get();
		
		mv.addObject("offre", offre);
		mv.setViewName("voirOffre");
		return mv;
	}
	
	@PostMapping(value = "/modifierOffre")
	public ModelAndView modifierOffre(ModelAndView mv, @RequestParam(value = "idoffre") String idoffre,
			@RequestParam(value = "nom") String nom, @RequestParam(value = "mail") String mail,
			@RequestParam(value = "tarif") String tarif, @RequestParam(value = "date") String date, 
			@RequestParam(value = "description") String description,
			@RequestParam(value = "adresse") String adresse, @RequestParam(value = "ville") String ville,
			@RequestParam(value = "pays") String pays, @RequestParam(value = "cp") String cp) {

		Offre offre = or.findById(Long.parseLong(idoffre)).get();
		offre.setNom(nom);
		offre.setMail(mail);
		offre.setTarif(Float.parseFloat(tarif));
		offre.setDate(LocalDate.parse(date));
		offre.setDescription(description);
		offre.getAdresse().setAdresse(adresse);
		offre.getAdresse().setVille(ville);
		offre.getAdresse().setPays(pays);
		offre.getAdresse().setCp(cp);
		offre.setClient(ClientController.getClient());

		or.save(offre);
		cr.save(ClientController.getClient());
		or.flush();
		
		
		ArrayList<Offre> listOffre = (ArrayList<Offre>) or.findAll();
		ArrayList<Offre> listFinal = new ArrayList<Offre>();
		
		
		for (Offre offre1 : listOffre) {
			if (offre1.getClient().getId() == ClientController.getClient().getId()) {
				listFinal.add(offre1);
			}
		}
		
		mv.addObject("listeOffre", listFinal);
		mv.setViewName("mesOffres");

		return mv;
	}
	
	@PostMapping(value = "/posterOffre")
	public ModelAndView posterOffre(ModelAndView mv, @RequestParam(value = "offreNom") String nom,
			@RequestParam(value = "inputEmail4") String mail, @RequestParam(value = "inputAddress") String adresse,
			@RequestParam(value = "inputCity") String ville, @RequestParam(value = "inputState") String pays, 
			@RequestParam(value = "inputZip") String cp, @RequestParam(value = "tarif") String tarif,
			@RequestParam(value = "date") String date, @RequestParam(value = "description") String description) {

		Adresse adr = new Adresse(adresse, ville, pays, cp);
		Offre offre = new Offre(nom, mail, Float.parseFloat(tarif), LocalDate.parse(date), description);
		offre.setAdresse(adr);
		offre.setClient(ClientController.getClient());

		or.save(offre);
		cr.save(ClientController.getClient());
		or.flush();
		mv.setViewName("accueil");

		return mv;
	}
	
	@GetMapping(value = "/supprimerOffre")
	public ModelAndView supprimerProduit(ModelAndView mv,@RequestParam(value = "id") String id) {
		
		or.deleteById(Long.parseLong(id));
		or.flush();
		
		ArrayList<Offre> listOffre = (ArrayList<Offre>) or.findAll();
		for (Offre offreLoop : listOffre) {
			if (offreLoop.getClient().getId() != ClientController.getClient().getId()) {
				listOffre.remove(offreLoop);
			}
		}
		mv.addObject("listeOffre", listOffre);
		mv.setViewName("mesOffres");
		return mv;
	}
	
}
