package fr.afpa.brics.controller;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import fr.afpa.beans.Authentification;
import fr.afpa.beans.Brics;
import fr.afpa.beans.Categorie;
import fr.afpa.repositories.dao.AuthentificationRepository;
import fr.afpa.repositories.dao.BricsRepository;
import fr.afpa.repositories.dao.CategorieRepository;

@Controller
public class BricsController {
	
	
	// PREUVE DE PULL
	
	
	@Autowired
	BricsRepository br;
	
	@Autowired
	CategorieRepository cr;
	
	@Autowired
	AuthentificationRepository ar;
	
	private static Brics brics;
	
	@GetMapping(value = "/inscriptionBrics")
	public String redirectFormBrics() {
		return "formulaireInscriptionBrics";
	}
	
	@GetMapping(value = "/trouverBrics")
	public String redirectFindBrics() {
		return "allCategories";
	}
	
	@GetMapping(value = "/jardinage")
	public String redirectjardinage() {
		return "listerBricsJardinage";
	}
	
	@GetMapping(value = "/bricolage")
	public String redirectBricolage() {
		return "listerBricsBricolage";
	}
	
	@GetMapping(value = "/peinture")
	public String redirectPeinture() {
		return "listerBricsPeinture";
	}
	
	@GetMapping(value = "/electromenager")
	public String redirectElectomenager() {
		return "listerBricsElectromenager";
	}
	
	@GetMapping(value = "/plomberie")
	public String redirectPlomberie() {
		return "listerBricsPlomberie";
	}
	
	@GetMapping(value = "/electricite")
	public String redirectElectricit�() {
		return "listerBricsElectricite";
	}


	
	/*@GetMapping(value = "/jardinage")
	public ModelAndView redirectJardinage(ModelAndView mv) {
		ArrayList<Brics> listBrics = (ArrayList<Brics>) br.findAll();
		ArrayList<Brics> listFinal = new ArrayList<Brics>();
		
		
		for (Brics brics : listBrics) {
			if (brics.get) {
				listFinal.add(offre);
			}
		}
		
		
		mv.addObject("listeOffre", listFinal);
		mv.setViewName("mesOffres");
		return mv;
	}*/
	
	

	@GetMapping(value = "/connexionBrics")
	public String redirectFormCoBrics() {
		return "formulaireConnexionBrics";
	}
	
	@GetMapping(value = "/accueilBrics")
	public String redirectAccueil() {
		return "accueilBrics";
	}
	
	@GetMapping(value = "/mesInfosBrics")
	public ModelAndView mesInfos(ModelAndView mv) {
		
		ArrayList<Categorie> allCategorie = (ArrayList<Categorie>) cr.findAll(); 
		ArrayList<Categorie> categorieBrics = new ArrayList<Categorie>();

		for (Categorie categorie : allCategorie) {
			
			if (categorie.getBrics().getId().equals(brics.getId())) {
				
				categorieBrics.add(categorie);
				
			}
			
		}
		
		System.out.println(allCategorie.get(0).getSousCategorie().get(0));
		
		
		
		mv.addObject("categories",categorieBrics);
		mv.addObject("brics",brics);
		mv.setViewName("infosBrics");
		return mv;
	}
	
	
	@GetMapping(value = "/mesCategoriesBrics")
	public ModelAndView mesCategories(ModelAndView mv) {

		ArrayList<Categorie> allCategorie = (ArrayList<Categorie>) cr.findAll(); 
		ArrayList<Categorie> categorieBrics = new ArrayList<Categorie>();
		

		for (Categorie categorie : allCategorie) {
			
			if (categorie.getBrics().getId().equals(brics.getId())) {
				
				categorieBrics.add(categorie);
				
			}
			
		}
		
		System.out.println(allCategorie.get(0).getSousCategorie().get(0));
		
		
		
		mv.addObject("categories",categorieBrics);
		mv.addObject("brics",brics);
		mv.setViewName("categoriesBrics");
		return mv;
	}
	
	@GetMapping(value = "/modifierBrics")
	public ModelAndView modifierBricsRedirect(ModelAndView mv, @RequestParam(value = "id") String id) {
		
		Brics bri = br.findById(Long.parseLong(id)).get();
		
		mv.addObject("brics", bri);
		mv.setViewName("modifierBrics");
		return mv;
	}
	
	@GetMapping(value = "/supprimerBrics")
	public ModelAndView supprimerProduit(ModelAndView mv,@RequestParam(value = "id") String id) {
		
		br.deleteById(Long.parseLong(id));
		br.flush();
		
		mv.setViewName("home");
		return mv;
	}
	
	@PostMapping(value = "/inscriptionBrics")
	public ModelAndView ajouterBrics(ModelAndView mv, @RequestParam(value = "nom") String nom,
			@RequestParam(value = "prenom") String prenom, @RequestParam(value = "mail") String mail, 
			@RequestParam(value = "dateNaissance") String dateNaissance, @RequestParam(value = "tel") String tel, 
			@RequestParam(value = "distance") String distance, @RequestParam(value = "tarif") String tarif, 
			@RequestParam(value = "login") String login, @RequestParam(value = "mdp") String mdp,
			@RequestParam(required = false, value = "serrure") String serrure, @RequestParam(required = false, value = "accrocheMurale") String accrocheMurale,
			@RequestParam(required = false, value = "pose") String pose, @RequestParam(required = false, value = "jardinerie") String jardinerie,
			@RequestParam(required = false, value = "plante") String plante, @RequestParam(required = false, value = "portail") String portail){

		Categorie bricolage = new Categorie();
		
		System.out.println("serrure " + serrure);
		System.out.println("accrocheMurale " + accrocheMurale);
		System.out.println("jardinerie " + jardinerie);
		System.out.println("pose " + pose);
		System.out.println("portail " + portail);
		
		Brics brics = new Brics(nom, prenom, mail, dateNaissance, tel, distance, tarif);
		Authentification authentification = new Authentification(login, mdp);
		brics.setAuthentification(authentification);

		

		if (serrure != null || accrocheMurale != null || pose != null ) {
			
			bricolage.setNom("Bricolage");
			
			ArrayList<String> sousCategorieBricolage = new ArrayList<String>();
			
			if (serrure != null) {		
				sousCategorieBricolage.add("Serrurerie");
			}
			
			if (accrocheMurale != null) {		
				sousCategorieBricolage.add("Accroche murale");
			}
			if (pose != null) {		
				sousCategorieBricolage.add("Pose ou r�paration store / fen�tre / porte");
			}
			
			bricolage.setSousCategorie(sousCategorieBricolage);
			
			bricolage.setBrics(brics);
			
		}
		
		br.saveAndFlush(brics);
		cr.saveAndFlush(bricolage);
		
		mv.setViewName("formulaireConnexionBrics");


		return mv;
	}
	
	@PostMapping(value = "/connexionBrics")
	public ModelAndView connexionBrics(ModelAndView mv, @RequestParam(value = "login") String login,
			@RequestParam(value = "mdp") String mdp) {

		ArrayList<Authentification> listAuth = (ArrayList<Authentification>) ar.findByLoginAndMdp(login, mdp);
		
		
		if (listAuth != null) {
			
			brics = listAuth.get(0).getBrics();
			System.out.println("good " + brics.getPrenom());
			
			mv.addObject("brics",brics);
			mv.setViewName("accueilBrics");
			
			return mv;
		}else {
			System.out.println("Aucun Brics en BDD");
		}
		return null;
	}

	@PostMapping(value = "/modifierBrics")
	public ModelAndView modifierBrics(ModelAndView mv, @RequestParam(value = "idbrics") String idclient,
			@RequestParam(value = "nom") String nom, @RequestParam(value = "prenom") String prenom,
			@RequestParam(value = "mail") String mail, @RequestParam(value = "dateNaissance") String dateNaissance,
			@RequestParam(value = "tel") String tel, @RequestParam(value = "distance") String distance,
			@RequestParam(value = "tarif") String tarif){

		Brics bri = br.findById(Long.parseLong(idclient)).get();
		bri.setNom(nom);
		bri.setPrenom(prenom);
		bri.setMail(mail);
		bri.setDateNaissance(dateNaissance);
		bri.setTel(tel);
		bri.setDistance(distance);
		bri.setTarif(tarif);


		br.save(bri);
		br.flush();
		
		
		mv.addObject(bri);
		mv.setViewName("infosBrics");

		return mv;
	}

	public static Brics getBrics() {
		return brics;
	}


	public static void setBrics(Brics brics) {
		BricsController.brics = brics;
	}
	
}
