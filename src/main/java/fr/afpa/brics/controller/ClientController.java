package fr.afpa.brics.controller;

import java.time.LocalDate;
import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import fr.afpa.beans.Authentification;
import fr.afpa.beans.Client;
import fr.afpa.beans.Offre;
import fr.afpa.repositories.dao.AuthentificationRepository;
import fr.afpa.repositories.dao.ClientRepository;
import fr.afpa.repositories.dao.OffreRepository;

@Controller
public class ClientController {
	
	@Autowired
	ClientRepository cr;
	
	@Autowired
	AuthentificationRepository ar;
	

	@Autowired
	OffreRepository or;
	
	private static Client client;
	
	@GetMapping(value = "/inscriptionClient")
	public String redirectForm() {
		return "formulaireInscriptionClient";
	}
	
	
	@GetMapping(value = "/accueil")
	public String redirectAccueil() {
		return "accueil";
	}
	

	@GetMapping(value = "/connexionClient")
	public String redirectFormCo() {
		return "formulaireConnexionClient";
	}
	
	@GetMapping(value = "/compteClient")
	public String compteClient() {
		if (client != null) {
			return "compteClient";
		}
		return "compteBrics";
	}
	
	@GetMapping(value = "/mesInfosClient")
	public ModelAndView mesInfos(ModelAndView mv) {

		mv.addObject("client",client);
		mv.setViewName("infosClient");
		return mv;
	}
	
	@GetMapping(value = "/contact")
	public ModelAndView contact(ModelAndView mv) {

		mv.setViewName("contact");
		return mv;
	}
	
	@GetMapping(value = "/modifierClient")
	public ModelAndView modifierClientRedirect(ModelAndView mv, @RequestParam(value = "id") String id) {
		
		Client cl = cr.findById(Long.parseLong(id)).get();
		
		mv.addObject("client", cl);
		mv.setViewName("modifierClient");
		return mv;
	}
	
	
	
	@PostMapping(value = "/inscriptionClient")
	public ModelAndView ajouterPersonne(ModelAndView mv, @RequestParam(value = "nom") String nom,
			@RequestParam(value = "prenom") String prenom, @RequestParam(value = "mail") String mail,
			@RequestParam(value = "login") String login, @RequestParam(value = "mdp") String mdp) {

		Client client = new Client(nom, prenom, mail);
		Authentification auth = new Authentification(login, BCrypt.hashpw(mdp, BCrypt.gensalt()));
		client.setAuth(auth);

		cr.save(client);
		cr.flush();
		
		mv.setViewName("formulaireConnexionClient");

		return mv;
	}
	
	@PostMapping(value = "/connexionClient")
	public ModelAndView connexion(ModelAndView mv, @RequestParam(value = "login") String login,
			@RequestParam(value = "mdp") String mdp) {

		
		ArrayList<Authentification> listAuthantification = (ArrayList<Authentification>) ar.findAll();
		
		if (listAuthantification != null) {
			
			for (Authentification authentification : listAuthantification) {
				
				if (login.equals(authentification.getLogin()) && BCrypt.checkpw(mdp, authentification.getMdp())) {
					
					client = authentification.getClient();
					System.out.println("good " + client.getPrenom());
					
					mv.addObject("client",client);
					mv.setViewName("accueil");
					
					return mv;
				}
			}
		
		}
		System.out.println("Aucun client en BDD");

		return null;
	}
	
	
	@GetMapping(value = "/supprimerClient")
	public ModelAndView supprimerClient(ModelAndView mv,@RequestParam(value = "id") String id) {
		
	ArrayList<Offre> listOffre = (ArrayList<Offre>) or.findAll();
		
		for (Offre offre : listOffre) {
			if (offre.getClient().getId().equals(Long.parseLong(id))) {
				or.deleteById(offre.getId());
				or.flush();
			}
		}
		
		cr.deleteById(Long.parseLong(id));
		cr.flush();
		
		mv.setViewName("home");
		return mv;
	}
	
	@PostMapping(value = "/modifierClient")
	public ModelAndView modifierClient(ModelAndView mv, @RequestParam(value = "idclient") String idclient,
			@RequestParam(value = "nom") String nom, @RequestParam(value = "prenom") String prenom,
			@RequestParam(value = "mail") String mail) {

		Client cl = cr.findById(Long.parseLong(idclient)).get();
		cl.setNom(nom);
		cl.setPrenom(prenom);
		cl.setMail(mail);

		cr.save(cl);
		cr.flush();
		
		
		mv.addObject("client", cl);
		mv.setViewName("infosClient");

		return mv;
	}

	
	
	
	

	public static Client getClient() {
		return client;
	}


	public static void setClient(Client client) {
		ClientController.client = client;
	}
	
}
